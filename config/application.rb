require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GcgGroup
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    ##============================================================##
    ## Ckeditor (not sure why as this was not needed for gcg-ground)
    ##============================================================##
    config.assets.precompile += Ckeditor.assets
    config.assets.precompile += %w( ckeditor/* )
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)

    ##============================================================##

    ##============================================================##
    ## Config action_mailer
    ##============================================================##
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => "utf-8"
    config.action_mailer.default content_type: "text/html"
    # ActionMailer::Base.default :content_type => "text/html"
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      address:              "smtp.gmail.com",
      port:                 587,
      domain:               "mail.google.com",
      user_name:            ENV["GMAIL_EMAIL"],
      password:             ENV["GMAIL_PASSWORD"],
      authentication:       :plain,
      enable_starttls_auto: true 
    }


    ##============================================================##
    ## Config paperclip S3_storage
    ##============================================================##
    config.paperclip_defaults = {
      :storage => :s3,
      :s3_protocol => :https,
      :s3_region => 'us-west-2',
      :s3_host_name => 's3-us-west-2.amazonaws.com', #newly needed as amazon changed its urls
      :s3_credentials => {
        :bucket => 'gcg-group',
        :access_key_id => ENV["S3_ACCESS_KEY_ID"],
        :secret_access_key => ENV["S3_SECRET_ACCESS_KEY"]
        }
      }

  end
end
