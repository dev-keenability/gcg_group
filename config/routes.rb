Rails.application.routes.draw do
    # require 'sidekiq/web'
  require 'admin_constraint'
  # mount Sidekiq::Web => '/sidekiq', constraints: AdminConstraint.new
  mount Ckeditor::Engine => '/ckeditor', constraints: AdminConstraint.new

  devise_scope :user do
    get "/users/sign_up",  :to => "devise/sessions#new"
  end  
  
  devise_for :users

  devise_scope :user do 
    get "/admin" => "devise/sessions#new" 
  end

  root 'home#index'

  # match '/about'                =>        'home#about',                 via: [:get],            :as => 'about'
  match '/careers'              =>        'home#careers',               via: [:get],            :as => 'careers'
  match '/purpose'              =>        'home#purpose',               via: [:get],            :as => 'purpose'
  match '/history'              =>        'home#history',               via: [:get],            :as => 'history'
  match '/why-choose-us'        =>        'home#why_choose_us',         via: [:get],            :as => 'why_choose_us'
  match '/contact'              =>        'home#contact',               via: [:get],            :as => 'contact'
  match '/privacy-policy'       =>        'home#privacy_policy',        via: [:get],            :as => 'privacy_policy'
  match '/locations/all-locations'          =>        'locations#all_locations',           via: [:get],            :as => 'all_locations'

  #########################################
  #==profiles
  #########################################
    get "/team", to: "profiles#index", as: :profiles
    post "/profiles", to: "profiles#create"
    get "/profiles/new", to: "profiles#new", as: :new_profile
    get "profiles/:id/edit", to: "profiles#edit", as: "edit_profile"
    patch "profiles/:id", to: "profiles#update"
    put "profiles/:id", to: "profiles#update"
    delete "profiles/:id", to: "profiles#destroy"

  #########################################
  #==locations url for location model
  #########################################
    get '/locations', to: 'locations#index', as: :locations
    post 'locations', to: 'locations#create'
    get '/locations/new', to: 'locations#new', as: :new_location
    get 'locations/:title_for_slug/edit', to: 'locations#edit', as: 'edit_location'
    get 'locations/:title_for_slug', to: 'locations#show', as: 'location'
    patch 'locations/:title_for_slug', to: 'locations#update'
    put 'locations/:title_for_slug', to: 'locations#update'
    delete 'locations/:title_for_slug', to: 'locations#destroy'

  #########################################
  #==press url for blog model
  #########################################
    get '/news', to: 'blogs#index', as: :blogs
    post 'news', to: 'blogs#create'
    get '/news/new', to: 'blogs#new', as: :new_blog
    get 'news/:title_for_slug/edit', to: 'blogs#edit', as: 'edit_blog'
    get 'news/:title_for_slug', to: 'blogs#show', as: 'blog'
    patch 'news/:title_for_slug', to: 'blogs#update'
    put 'news/:title_for_slug', to: 'blogs#update'
    delete 'news/:title_for_slug', to: 'blogs#destroy'

  #########################################
  #==press url for blog model
  #########################################
    get '/press', to: 'bligs#index', as: :bligs
    post 'press', to: 'bligs#create'
    get '/press/new', to: 'bligs#new', as: :new_blig
    get 'press/:title_for_slug/edit', to: 'bligs#edit', as: 'edit_blig'
    get 'press/:title_for_slug', to: 'bligs#show', as: 'blig'
    patch 'press/:title_for_slug', to: 'bligs#update'
    put 'press/:title_for_slug', to: 'bligs#update'
    delete 'press/:title_for_slug', to: 'bligs#destroy'

  match "/contacts"              =>        "contacts#create",              via: [:post]
  match "/applicants"            =>        "applicants#create",            via: [:post]
  
  get '/sitemap.xml', to: redirect("https://s3-us-west-2.amazonaws.com/gcg-group/sitemaps/sitemap.xml.gz", status: 301)

  match "*path", to: "home#catch_all", via: :all
end
