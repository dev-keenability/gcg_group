# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.gcggroup.com"

SitemapGenerator::Sitemap.adapter = SitemapGenerator::AwsSdkAdapter.new(
  "gcg-group",
  aws_access_key_id: ENV["S3_ACCESS_KEY_ID"],
  aws_secret_access_key: ENV["S3_SECRET_ACCESS_KEY"],
  aws_region: 'us-west-2'
)

SitemapGenerator::Sitemap.sitemaps_host = "https://s3-us-west-2.amazonaws.com/gcg-group/"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
  add blogs_path, :priority => 0.8, :changefreq => 'weekly'
  Blog.find_each do |blog|
    add blog_path(blog.title_for_slug), :changefreq => 'weekly', :lastmod => blog.updated_at, :priority => 0.8
  end
  add locations_path, :priority => 0.8, :changefreq => 'weekly'
  add bligs_path, :priority => 0.8, :changefreq => 'weekly'
  Blig.find_each do |blig|
    add blig_path(blig.title_for_slug), :changefreq => 'monthly', :lastmod => blig.updated_at, :priority => 0.8
  end

  add privacy_policy_path, :priority => 0.8, :changefreq => 'monthly'
  add contact_path, :priority => 0.8, :changefreq => 'monthly'
  add why_choose_us_path, :priority => 0.8, :changefreq => 'monthly'
  add history_path, :priority => 0.8, :changefreq => 'monthly'
  add purpose_path, :priority => 0.8, :changefreq => 'monthly'
  add careers_path, :priority => 0.8, :changefreq => 'monthly'

end
