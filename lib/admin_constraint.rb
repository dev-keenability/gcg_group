class AdminConstraint

  # AdminConstraint must respond to :call or :matches?
  def matches?(request)
    return false if request.session["warden.user.user.key"].nil?
    return false unless request.session["warden.user.user.key"][0][0] #this is how you find user_id from sessions hash using devise, if normal authentiacation it would just be session[:user_id]
    user = User.find request.session["warden.user.user.key"][0][0]
    user && user.admin? #User must be an admin is one of the requirements
  end

end