class AddGcgToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :gcg_ground, :boolean
    add_column :locations, :gcg_events, :boolean
    add_column :locations, :gcg_catering, :boolean
  end
end
