class CreateBligs < ActiveRecord::Migration[5.0]
  def change
    
    create_table :bligs do |t|
      t.string :title
      t.text :content
      t.text :content_index
      t.string :title_for_slug, unique: true
      t.string :main_image
      t.datetime :pubdate
      t.string :meta_description
      t.string :meta_keywords

      t.timestamps
    end

    add_index(:bligs, [:title_for_slug], {:unique => true})

  end
end