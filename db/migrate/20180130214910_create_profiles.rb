class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string :full_name
      t.string :title
      t.string :telephone
      t.string :mobile
      t.string :email
      t.integer :position

      t.timestamps
    end
  end
end
