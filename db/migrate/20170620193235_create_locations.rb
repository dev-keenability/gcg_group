class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :name
      t.string :title_for_slug

      t.timestamps
    end
    add_index :locations, :title_for_slug
  end
end
