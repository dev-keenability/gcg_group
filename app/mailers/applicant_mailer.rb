class ApplicantMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]

  def welcome_email(applicant, name, brand)
    @applicant = applicant
    @brand = brand.humanize
    myfilename = name
    attachments["#{@applicant.full_name}_resume.pdf"] = File.read("#{Rails.root}/tmp/uploaded_resume/#{myfilename}")
    if Rails.env.production?
      mail(to: 'lynda.pantoja@goddardcatering.com', bcc: 'dev@keenability.com', subject: 'New applicant for GCG-Group')
    else
      mail(to: 'alex@keenability.com', bcc: 'dev@keenability.com', subject: 'New applicant for GCG-Group')
    end
  end
end
