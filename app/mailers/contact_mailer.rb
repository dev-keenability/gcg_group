class ContactMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]

  def welcome_email(contact)
    @contact = contact
    mail(to: ['stewart.massiah@goddardcatering.com', 'francisco.mayorga@goddardcatering.com', 'robbie.medina@goddardcatering.com', 'bruno.barrientos@goddardcatering.com'], subject: 'New contact for GCG-Group')
  end
end
