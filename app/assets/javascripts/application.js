// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require plugins/jquery.readySelector
//= require ckeditor/init
//= require bootstrap
//= require light-gallery
//= require owl.carousel.min
//= require turbolinks
//= require_tree .

$(document).on('turbolinks:load', function() {
  if ($('body.home.index').length > 0) {
    console.log('index');
    // $('.carousel').carousel({
    //   interval: 5000
    // })
  }

  if ($('body.home.about').length > 0) {
    console.log('about');
  }

    // I have 3 different views using the following functions, therefore you will see the id's separated with commas
    if ($('body.profiles.new').length > 0 || $('body.profiles.edit').length > 0) {
      $("#profile_image_attributes_pic").change(function(evt) { 
        if (evt.target.files && evt.target.files[0]) {
            var reader = new FileReader();

            reader.onload = function (event) {
                $('#current_image')
                    .attr('src', event.target.result)
                    .width(300)
                    .height(169);

                $('#square_image')
                    .attr('src', event.target.result)
                    .width(300)
                    .height(300);

                $('#location_image')
                    .attr('src', event.target.result)
                    .width(500)
                    .height(250);
            };

            reader.readAsDataURL(evt.target.files[0]);
        }
      });
    } 





          // Initialize google maps javascript api
    if ($('body.home.index').length > 0 || $('body.locations.index').length > 0 || $('body.home.contact').length > 0 || $('body.locations.all_locations').length > 0 || $('body.home.catch_all').length > 0) {    //need to load on many pages
        var initMap = function() {
          
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 3,
                scrollwheel: false,
                center: new google.maps.LatLng(19.466766,-70.2060925)
                // mapTypeId: 'terrain'
            });

            var infowindow;   

            // This is how we get the locations as a javascript object
            var locations = $('#myid').data('mylocations');
            // console.log(locations[0].name);
            // console.log(locations[0].latitude);
            // console.log(locations[0].longitude);

            function addMarker(location) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(location.latitude,location.longitude),
                    icon: 'https://s3-us-west-2.amazonaws.com/gcg-group/general/gcg-group-map-icon.png',
                    optimized: false,
                    // zIndex:Date.now(), //so that last markers in the loop overlay the earlier ones
                    map: map
                    // title: feature.title
                });
                // We are declaring the content attribute to the marker so that we can reference it later with on click (this)
                marker.content = "<p class='mapp'>"+ location.name + "</p>"; 
                marker.content += (location.gcg_catering) ? "<a href='http://www.gcg-catering.com' target='_blank'>" + "<h6 class='mapcatering'>" + "GCG Catering" + "</h6>" + "</a>" : ""; 
                marker.content += (location.gcg_events) ? "<a href='http://www.gcg-events.com' target='_blank'>" + "<h6 class='mapevents'>" + "GCG Events" + "</h6>" + "</a>" : ""; 
                marker.content += (location.gcg_ground) ? "<a href='http://www.gcg-ground.com' target='_blank'>" + "<h6 class='mapground'>" + "GCG Ground" + "</h6>" + "</a>" : ""; 
                infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, 'mouseover', function() {
                    if(!marker.open){
                        infowindow.setContent(this.content);
                        infowindow.open(map,marker);
                        marker.open = true;
                    }
                    else{
                        infowindow.close();
                        marker.open = false;
                    }
                    google.maps.event.addListener(map, 'mouseout', function() {
                        infowindow.close();
                        marker.open = false;
                    });
                });
            }


            for (var i = 0, location; location = locations[i]; i++) {
              addMarker(location);
            }

        }

        initMap() // Call initMap here to avoid initMap is not a function error
    } // home.index








});