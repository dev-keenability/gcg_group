class ContactsController < ApplicationController
  def create
    @contact = Contact.new(contact_params)

    if @contact.save
      # Tell the contactMailer to send a welcome email after save
      ContactMailer.welcome_email(@contact).deliver_now
      flash[:success] = 'Thank you for contacting us.'
      redirect_to root_path(anchor: 'maincontent')
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :phone_number, :message)
  end
end
