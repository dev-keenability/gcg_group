class HomeController < ApplicationController
  
  def index
    @blogs = Blog.limit(3)
    @locations = Location.all
  end

  def careers
  end

  def purpose
  end

  def history
  end

  def why_choose_us
  end

  def contact
    @locations = Location.all
  end

  def privacy_policy
  end

  def terms
  end

  def catch_all
    flash.now[:warning] = "The page or request you were looking for was not found."
    @blogs = Blog.limit(3)
    @locations = Location.all
    render :index
  end

end
