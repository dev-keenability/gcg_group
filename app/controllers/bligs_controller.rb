class BligsController < ApplicationController
  before_action :set_blig, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  def index
    @bligs = Blig.order(pubdate: :desc)
  end

  def show
  end

  def new
    @blig = Blig.new
  end

  def edit
  end

  def create
    @blig = Blig.new(blig_params)
    if @blig.save
      redirect_to blig_path(@blig.title_for_slug), notice: 'Press was successfully created.'
    else
      render :new
    end
  end

  def update
    if @blig.update(blig_params)
      redirect_to blig_path(@blig.title_for_slug), notice: 'Press was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @blig.destroy
    redirect_to bligs_url, notice: 'Press was successfully destroyed.'
  end

  private

    def set_blig
      @blig = Blig.find_by(title_for_slug: params[:title_for_slug])
    end

    def blig_params
      params.require(:blig).permit(:title, :content, :content_index, :title_for_slug, :main_image, :pubdate, :meta_description, :meta_keywords, images_attributes: [:id, :pic, :name, :_destroy])
    end

end
