class BlogsController < ApplicationController
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  def index
    @blogs = Blog.order(pubdate: :desc)
  end

  def show
  end

  def new
    @blog = Blog.new
  end

  def edit
  end

  def create
    @blog = Blog.new(blog_params)
    if @blog.save
      redirect_to blog_path(@blog.title_for_slug), notice: 'News was successfully created.'
    else
      render :new
    end
  end

  def update
    if @blog.update(blog_params)
      redirect_to blog_path(@blog.title_for_slug), notice: 'News was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @blog.destroy
    redirect_to blogs_url, notice: 'News was successfully destroyed.'
  end

  private

    def set_blog
      @blog = Blog.find_by(title_for_slug: params[:title_for_slug])
    end

    def blog_params
      params.require(:blog).permit(:title, :content, :content_index, :title_for_slug, :main_image, :pubdate, :meta_description, :meta_keywords, images_attributes: [:id, :pic, :name, :_destroy])
    end

end
