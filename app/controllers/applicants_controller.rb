class ApplicantsController < ApplicationController
  def create
    @applicant = Applicant.new
    @applicant.full_name = params[:applicant][:full_name]
    @applicant.email = params[:applicant][:email]
    @applicant.location_id = params[:applicant][:location_id]
    @applicant.phone_number = params[:applicant][:phone_number]

      # File needs to be pdf
      if params[:applicant][:resume].original_filename.split(".").last.squish.downcase != "pdf" || params[:applicant][:resume].content_type != "application/pdf"
        flash[:alert] = "File needs to be type pdf"        
        redirect_to :back and return
      end 
      
      #Uploading file (f.file_field comes with 4 methods 1: original_filename, 2: tempfile, 3: content_type, 4: headers) 
      name = SecureRandom.hex + "_" + params[:applicant][:resume].original_filename
      folder = "tmp/uploaded_resume" 
      FileUtils.mkdir_p(folder) if !File.directory?(folder)
      path = File.join(folder, name)
      File.open(path, "wb") { |f| f.write(params[:applicant][:resume].read) }

      brand = params[:brand]

    if @applicant.save
      ApplicantMailer.welcome_email(@applicant, name, brand).deliver_later
      flash[:success] = 'Thank you for your application.'
      redirect_to root_path(anchor: 'maincontent')
    end

  end

  private

  def applicant_params
    params.require(:applicant).permit(:full_name, :email, :phone_number, :location_id)
  end
end
