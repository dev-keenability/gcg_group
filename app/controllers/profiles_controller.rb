class ProfilesController < ApplicationController
  before_action :set_profile, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]

  def index
    @profiles = Profile.order(position: :asc)
  end

  def new
    @profile = Profile.new
    @profile.build_image
  end
 
  def edit
  end

  def create
    @profile = Profile.new(profile_params)
    if @profile.save
      redirect_to profiles_path, notice: 'Profile was successfully created.'
    else
      render :new
    end
  end

  def update
    if @profile.update(profile_params)
      redirect_to profiles_path(@profile), notice: 'Profile was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @profile.destroy
    redirect_to profiles_url, notice: 'profile was successfully destroyed.'
  end

  private

    def set_profile
      @profile = Profile.find(params[:id])
    end

    def profile_params
      params.require(:profile).permit(:full_name, :title, :telephone, :mobile, :email, :position, image_attributes: [:id, :pic, :name])
    end
end

