class Blig < ApplicationRecord

  before_save :title_url_it
  before_save :remove_img

  has_many :images, as: :imageable, :class_name => "Blig::Asset", dependent: :destroy
  accepts_nested_attributes_for :images, allow_destroy: true
  
  #This is for SEO purposes, you want the url to be the title of the blig instead of its ID
  def title_url_it
    self.title_for_slug = title.downcase.squish.parameterize("-")
  end

  def remove_img
    nokogiri_html_document =  Nokogiri::HTML(self.content) #heroku does not like tmp directory
    nokogiri_html_document.search('img').each do |img|
      img.remove
    end
    self.content_index = nokogiri_html_document
    #note that if you call self.save here, you might run into infinite loops (stack level too deep), even for after_save, after_create and after_update callbacks
  end

end