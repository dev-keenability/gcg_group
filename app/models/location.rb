class Location < ApplicationRecord
  has_many :applicants
  
  geocoded_by :address
  after_validation :geocode, :if => :address_changed?
  
  before_save :title_url_it

  def title_url_it
    self.title_for_slug = name.downcase.squish.parameterize("-")
  end
end