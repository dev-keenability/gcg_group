class Profile < ApplicationRecord
  has_one :image, as: :imageable, :class_name => "Profile::Asset", dependent: :destroy
  accepts_nested_attributes_for :image, update_only: true
end
